package me.admund.framework.android;

import android.app.Activity;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;

public class MoPubAdsHelper implements MoPubInterstitial.InterstitialAdListener {
    private static final long MIN_SHOW_ADS_TIME = 1 * 60 * 1000; // 1min
    private long lastShowAdsTime = 0;

    private boolean isLoading = false;

    private MoPubInterstitial moPubInterstitial = null;

    public MoPubAdsHelper(Activity activity) {
        moPubInterstitial = new MoPubInterstitial(activity, "4acc21cd4bbf441a9d4dacc573172af0");
        moPubInterstitial.setInterstitialAdListener(this);

        loadAds();
    }

    public boolean showFullScreenAds() {
//        Log.e("MoPub", "showFullScreenAds " + moPubInterstitial + " " + moPubInterstitial.isReady() + " " + canShowAds());
        if (moPubInterstitial.isReady() && canShowAds()) {
            moPubInterstitial.show();
            lastShowAdsTime = System.currentTimeMillis();
            return true;
        }

        if (!moPubInterstitial.isReady() && !isLoading) {
            loadAds();
        }

        return false;
    }

    @Override
    public void onInterstitialLoaded(MoPubInterstitial moPubInterstitial) {
//        Log.e("MoPub", "onInterstitialLoaded " + moPubInterstitial.isReady());
        isLoading = false;
    }

    @Override
    public void onInterstitialFailed(MoPubInterstitial moPubInterstitial, MoPubErrorCode moPubErrorCode) {
//        Log.e("MoPub", "onInterstitialFailed " + moPubErrorCode);
        isLoading = false;
        Answers.getInstance().logCustom(new CustomEvent("ADS_FAILED"));
    }

    @Override
    public void onInterstitialShown(MoPubInterstitial moPubInterstitial) {
//        Log.e("MoPub", "onInterstitialShown " + moPubInterstitial.isReady());
        Answers.getInstance().logCustom(new CustomEvent("ADS_SHOWN"));
    }

    @Override
    public void onInterstitialClicked(MoPubInterstitial moPubInterstitial) {
//        Log.e("MoPub", "onInterstitialClicked " + moPubInterstitial.isReady());
        loadAds();
        Answers.getInstance().logCustom(new CustomEvent("ADS_CLICKED"));
    }

    @Override
    public void onInterstitialDismissed(MoPubInterstitial moPubInterstitial) {
//        Log.e("MoPub", "onInterstitialDismissed " + moPubInterstitial.isReady());
        loadAds();
        Answers.getInstance().logCustom(new CustomEvent("ADS_DISMISSED"));
    }

    private void loadAds() {
        isLoading = true;
        moPubInterstitial.load();
    }

    private boolean canShowAds() {
        return System.currentTimeMillis() - lastShowAdsTime > MIN_SHOW_ADS_TIME;
    }
}
