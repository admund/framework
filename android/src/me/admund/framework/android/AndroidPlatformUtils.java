package me.admund.framework.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import me.admund.framework.achievements.AchievementState;
import me.admund.framework.achievements.AchievementsBuilder;
import me.admund.framework.achievements.AchievementsManager;
import me.admund.framework.achievements.IPlatformUtils;
import me.admund.framework.analytics.AnalyticEvent;
import me.admund.framework.utils.Pair;

public class AndroidPlatformUtils implements IPlatformUtils, GameHelper.GameHelperListener {
    private Activity activity = null;
    private GameHelper gameHelper = null;
    private AchievementsBuilder achievementsBuilder = null;
    private MoPubAdsHelper moPubAdsHelper = null;

    public AndroidPlatformUtils(Activity activity, AchievementsBuilder achievementsBuilder) {
        this.activity = activity;
        this.achievementsBuilder = achievementsBuilder;

        gameHelper = new GameHelper(activity, GameHelper.CLIENT_GAMES);
        gameHelper.enableDebugLog(false);
        gameHelper.setup(this);

        moPubAdsHelper = new MoPubAdsHelper(activity);
    }

    public void onStart(Activity activity) {
        gameHelper.onStart(activity);
    }

    public void onStop() {
        gameHelper.onStop();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        gameHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void signIn() {
        try {
            activity.runOnUiThread(new Runnable() {
                //@Override
                public void run() {
                    gameHelper.beginUserInitiatedSignIn();
                }
            });
        } catch (Exception e) {
            Gdx.app.log("MainActivity", "Log in failed: " + e.getMessage() + ".");
        }
    }

    @Override
    public void signOut() {
        try {
            activity.runOnUiThread(new Runnable() {
                //@Override
                public void run() {
                    gameHelper.signOut();
                }
            });
        } catch (Exception e) {
            Gdx.app.log("MainActivity", "Log out failed: " + e.getMessage() + ".");
        }
    }

    @Override
    public void rateGame() {
        String str = "https://play.google.com/store/apps/details?id=" + activity.getPackageName();
        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str)));
    }

    @Override
    public void showLeaderboard() {
        if (isSignedIn()) {
            Intent intent = Games.Leaderboards.getAllLeaderboardsIntent(gameHelper.getApiClient());
            activity.startActivityForResult(intent, 1);
        } else {
            signIn();
        }
    }

    @Override
    public void showAchievements() {
        if (isSignedIn()) {
            Intent intent = Games.Achievements.getAchievementsIntent(gameHelper.getApiClient());
            activity.startActivityForResult(intent, 1);
        } else {
            signIn();
        }
    }

    @Override
    public void createAchievements(AchievementsManager manager) {
        achievementsBuilder.addAchievements(manager);
    }

    @Override
    public boolean submitScore(int score, String guestName, String leaderboardName) {
        if (isSignedIn()) {
            Games.Leaderboards.submitScore(gameHelper.getApiClient(), leaderboardName, score);
            return true;
        }
        return false;
    }

    @Override
    public boolean earnAchievement(String achievementId) {
        if (isSignedIn()) {
            Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
        }
        return false;
    }

    @Override
    public boolean incrementAchievement(String achievementId, int incrementValue) {
        if (isSignedIn()) {
            Games.Achievements.increment(gameHelper.getApiClient(), achievementId, incrementValue);
        }
        return false;
    }

    @Override
    public Array<AchievementState> getAchievementsStates() {
        return new Array<AchievementState>(); // TODO achievement state should be get from GooglePlay
    }

    @Override
    public void showScores() {
    }

    @Override
    public boolean isSignedIn() {
        return gameHelper.isSignedIn();
    }

    @Override
    public void sendAnalyticsEvent(AnalyticEvent event) {
        CustomEvent fabricEvent = new CustomEvent(event.getEventName());
        for (Pair<String, String> attributePair : event.getStringAttributesList()) {
            fabricEvent.putCustomAttribute(attributePair.first, attributePair.second);
        }
        for (Pair<String, Number> attributePair : event.getNumberAttributesList()) {
            fabricEvent.putCustomAttribute(attributePair.first, attributePair.second);
        }
        Answers.getInstance().logCustom(fabricEvent);
    }

    @Override
    public boolean showFullScreenAd() {
        return moPubAdsHelper.showFullScreenAds();
    }

    @Override
    public void onSignInFailed() {
    }

    @Override
    public void onSignInSucceeded() {
    }

    @Override
    public void vibrate() {
        Vibrator vibrator = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator != null) {
            vibrator.vibrate(200);
        }
    }
}
