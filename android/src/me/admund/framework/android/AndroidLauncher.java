package me.admund.framework.android;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import me.admund.framework.FrameworkTest;
import me.admund.framework.game.AbstractGame;

public class AndroidLauncher extends AndroidApplication {

    private static final long EXIT_DELAY = 1500; // 1,5s

    private long lastBackPressed = 0;
    private AndroidPlatformUtils provider = null;
    private AbstractGame game = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.numSamples = 2;
        config.hideStatusBar = true;
        config.useAccelerometer = false;
        config.useCompass = false;
        game = new FrameworkTest(null);
        initialize(game, config);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (provider != null) {
            provider.onStart(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (provider != null) {
            game.pause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (provider != null) {
            provider.onStop();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (provider != null) {
            provider.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (game != null && game.backPressed()) {
            if (System.currentTimeMillis() - lastBackPressed < EXIT_DELAY) {
                game.setToDispose();
                finish();
            } else {
                lastBackPressed = System.currentTimeMillis();
                Toast.makeText(this, "Do you really want to exit game?", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
