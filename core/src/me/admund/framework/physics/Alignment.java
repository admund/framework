package me.admund.framework.physics;

/**
 * Created by admund on 2015-08-10.
 */
public class Alignment {
    public static final Alignment CENTER = new Alignment(.5f, .5f);
    public static final Alignment LEFT_TOP = new Alignment(0f, 1f);
    public static final Alignment LEFT_BOTTOM = new Alignment(0f, 0f);
    public static final Alignment RIGHT_TOP = new Alignment(1f, 1f);
    public static final Alignment RIGHT_BOTTOM = new Alignment(1f, 0f);
    public static final Alignment TOP = new Alignment(.5f, 1f);
    public static final Alignment BOTTOM = new Alignment(.5f, 0f);
    public static final Alignment LEFT = new Alignment(0f, .5f);
    public static final Alignment RIGHT = new Alignment(1f, .5f);

    private float alignmentX = 0;
    private float alignmentY = 0;

    public static Alignment create(float alignmentX, float alignmentY) {
        return new Alignment(alignmentX, alignmentY);
    }

    public Alignment(float alignmentX, float alignmentY) {
        this.alignmentX = alignmentX;
        this.alignmentY = alignmentY;
    }

    public float getAlignmentX() {
        return alignmentX;
    }

    public float getAlignmentY() {
        return alignmentY;
    }
}
