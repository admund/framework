package me.admund.framework.physics;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.draw.model.BodyEditorLoader;

public class ModelPhysicsLoader extends AsynchronousAssetLoader<BodyEditorLoader, ModelPhysicsLoader.ModelPhysicsParameter> {

    private BodyEditorLoader bodyEditorLoader = null;

    public ModelPhysicsLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, ModelPhysicsLoader.ModelPhysicsParameter parameter) {
        bodyEditorLoader = new BodyEditorLoader(file);
    }

    @Override
    public BodyEditorLoader loadSync(AssetManager manager, String fileName, FileHandle file, ModelPhysicsLoader.ModelPhysicsParameter parameter) {
        BodyEditorLoader bodyEditorLoader = this.bodyEditorLoader;
        this.bodyEditorLoader = null;
        return bodyEditorLoader;
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, ModelPhysicsLoader.ModelPhysicsParameter parameter) {
        return null;
    }

    static public class ModelPhysicsParameter extends AssetLoaderParameters<BodyEditorLoader> {
    }
}
