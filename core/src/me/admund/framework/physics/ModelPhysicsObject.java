package me.admund.framework.physics;

import com.badlogic.gdx.math.Vector2;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.model.BodyEditorLoader;

public abstract class ModelPhysicsObject extends PhysicsObject {

    protected void loadModel(String modelName) {
        destroyFixtures();
        BodyEditorLoader loader = GameUtils.assetsManager.getBodyEditorLoader(modelName);
        loader.attachFixture(getBody(), null, getFixtureDef(), getWidth());
        Vector2 origin = loader.getOrigin(null, 1);//getWidth());
        setOrigin(origin.x * getWidth(), origin.y * getWidth());
        setAlignment(Alignment.create(origin.x, origin.y * (getWidth() / getHeight())));
        setFixture(getBody().getFixtureList().first());
    }

    private void destroyFixtures() {
        while (getBody().getFixtureList().size > 0) {
            getBody().destroyFixture(this.getBody().getFixtureList().first());
        }
    }
}
