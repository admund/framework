package me.admund.framework.analytics;


import me.admund.framework.utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class AnalyticEvent {
    private final String eventName;
    private final List<Pair<String, String>> stringAttributesList = new ArrayList<Pair<String, String>>();
    private final List<Pair<String, Number>> numberAttributesList = new ArrayList<Pair<String, Number>>();

    public AnalyticEvent(String eventName) {
        this.eventName = eventName;
    }

    public AnalyticEvent addAttribut(String attributeName, String attributeValue) {
        stringAttributesList.add(new Pair<String, String>(attributeName, attributeValue));
        return this;
    }

    public AnalyticEvent addAttribut(String attributeName, Number attributeValue) {
        numberAttributesList.add(new Pair<String, Number>(attributeName, attributeValue));
        return this;
    }

    public String getEventName() {
        return eventName;
    }

    public List<Pair<String, String>> getStringAttributesList() {
        return stringAttributesList;
    }

    public List<Pair<String, Number>> getNumberAttributesList() {
        return numberAttributesList;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name ");
        builder.append(eventName);
        for (Pair<String, String> attributePair : getStringAttributesList()) {
            builder.append(" ");
            builder.append(attributePair.first);
            builder.append("=");
            builder.append(attributePair.second);
        }
        for (Pair<String, Number> attributePair : getNumberAttributesList()) {
            builder.append(" ");
            builder.append(attributePair.first);
            builder.append("=");
            builder.append(attributePair.second);
        }
        return builder.toString();
    }
}
