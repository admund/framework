package me.admund.framework.sounds;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import me.admund.framework.GameUtils;

public abstract class FrameworkSoundsManager implements Disposable {
    private ObjectMap<String, Music> musicsMap = null;
    private ObjectMap<String, Sound> soundsMap = null;
    private Music mainTheme = null;

    public FrameworkSoundsManager() {
        musicsMap = new ObjectMap<String, Music>();
        soundsMap = new ObjectMap<String, Sound>();
    }

    public abstract void init();

    public void playMain() {
        mainTheme.play();
    }

    public Sound getSound(String soundName) {
        return soundsMap.get(soundName);
    }

    public long playSound(String soundName) {
        Sound sound = soundsMap.get(soundName);
        if(sound != null) {
            return sound.play();
        }
        return -1;
    }

    public long playSound(String soundName, long soundId) {
        Sound sound = soundsMap.get(soundName);
        if(sound != null && soundId != -1) {
            return sound.play(soundId);
        }
        return -1;
    }

    public void stopSound(String soundName) {
        Sound sound = soundsMap.get(soundName);
        if(sound != null) {
            sound.stop();
        }
    }

    public void stopSound(String soundName, long soundId) {
        Sound sound = soundsMap.get(soundName);
        if(sound != null && soundId != -1) {
            sound.stop(soundId);
        }
    }

    protected void setMainTheme(String mainThemeName) {
        if(mainThemeName != null) {
            mainTheme = GameUtils.assetsManager.getMusic(mainThemeName);
            mainTheme.setLooping(true);
        }
    }

    protected void addSound(String soundName) {
        soundsMap.put(soundName, GameUtils.assetsManager.getSound(soundName));
    }

    protected void addMusic(String musicName) {
        musicsMap.put(musicName, GameUtils.assetsManager.getMusic(musicName));
    }

    @Override
    public void dispose() {
        ObjectMap.Values<Music> musicValues = musicsMap.values();
        if(musicValues.hasNext()) {
            musicValues.next().dispose();
        }

        ObjectMap.Values<Sound> soundsValues = soundsMap.values();
        if(soundsValues.hasNext()) {
            soundsValues.next().dispose();
        }
    }
}
