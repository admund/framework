package me.admund.framework.utils;

public class MFloat {
    private float mFloat = 0;

    public float getFloatValue() {
        return mFloat;
    }

    public void setFloatValue(float mFloat) {
        this.mFloat = mFloat;
    }

    public void add(float addValue) {
        this.mFloat += addValue;
    }
}
