package me.admund.framework.logic;

import com.badlogic.gdx.Preferences;

public abstract class GamePreferences {
    protected Preferences preferences = null;

    public GamePreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public abstract void init();

    public abstract void save(Score score);

    public String getString(String propertyKey) {
        return preferences.getString(propertyKey, null);
    }

    public void putString(String propertyKey, String propertyValue) {
        preferences.putString(propertyKey, propertyValue);
        flush();
    }

    public void flush() {
        preferences.flush();
    }
}
