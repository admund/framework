package me.admund.framework.logic;

import com.badlogic.gdx.utils.ObjectMap;

import me.admund.framework.utils.MFloat;

public class Score {
    private GamePreferences pref = null;

    private ObjectMap<String, MFloat> scoreMap = new ObjectMap<String, MFloat>();

    public void init(GamePreferences pref) {
        this.pref = pref;
        this.pref.init();
    }

    public float getScore(String scoreName) {
        MFloat tmp = scoreMap.get(scoreName);
        if(tmp == null) {
            tmp = new MFloat();
            scoreMap.put(scoreName, tmp);
        }
        return tmp.getFloatValue();
    }

    public void addScore(String scoreName, float value) {
        MFloat tmp = scoreMap.get(scoreName);
        if(tmp == null) {
            tmp = new MFloat();
            scoreMap.put(scoreName, tmp);
        }
        tmp.add(value);
    }

    public void setScore(String scoreName, float value) {
        MFloat tmp = scoreMap.get(scoreName);
        if(tmp == null) {
            tmp = new MFloat();
            scoreMap.put(scoreName, tmp);
        }
        tmp.setFloatValue(value);
    }

    public void clear() {
        scoreMap.clear();
    }

    public void save() {
        if(pref != null) {
            pref.save(this);
        }
    }
}
