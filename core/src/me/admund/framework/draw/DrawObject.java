package me.admund.framework.draw;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.draw.holders.ISpriteHolder;
import me.admund.framework.physics.PhysicsWorld;
import me.admund.framework.utils.UpdateType;

public abstract class DrawObject extends Actor {
    protected ISpriteHolder spriteHolder = null;
    protected Array<DrawObject> childArray = new Array<DrawObject>();

    @Override
    public void act(float delta) {
        super.act(delta);
        if(spriteHolder != null) {
            spriteHolder.act(delta);
        }

        for (int i = 0; i < childArray.size; i++) {
            childArray.get(i).act(delta);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(isVisible() && isSpriteHolder()) {
            spriteHolder.draw(batch, parentAlpha);
        }

        for (int i = 0; i < childArray.size; i++) {
            childArray.get(i).draw(batch, parentAlpha);
        }
    }

    @Override
    public void setSize(float width, float height) {
        super.setSize(width, height);
        updateSpriteHolder(UpdateType.SIZE);
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        updateSpriteHolder(UpdateType.POSSITION);
    }

    @Override
    public void setOrigin(int alignment) {
        super.setOrigin(alignment);
        updateSpriteHolder(UpdateType.ALL);
    }

    @Override
    public void setOrigin(float originX, float originY) {
        super.setOrigin(originX, originY);
        updateSpriteHolder(UpdateType.ALL);
    }

    @Override
    public void setScale(float scaleX, float scaleY) {
        super.setScale(scaleX, scaleY);
        updateSpriteHolder(UpdateType.ALL);
    }

    @Override
    public void moveBy(float x, float y) {
        super.moveBy(x, y);
        updateSpriteHolder(UpdateType.POSSITION);
    }

    public void changeAnimationState(AnimationState state) {
        spriteHolder.changeAnimationState(state);
    }

    public float getScreenWidth() {
        return getWidth() * PhysicsWorld.BOX_TO_SCREEN;
    }

    public float getScreenHeight() {
        return getHeight() * PhysicsWorld.BOX_TO_SCREEN;
    }

    public float getScreenPosX() {
        return getX() * PhysicsWorld.BOX_TO_SCREEN;
    }

    public float getScreenPosY() {
        return getY() * PhysicsWorld.BOX_TO_SCREEN;
    }

    protected boolean isSpriteHolder() {
        return spriteHolder != null;
    }

    protected void setSpriteHolder(ISpriteHolder spriteHolder, boolean useSpriteSize) {
        this.spriteHolder = spriteHolder;
        if(useSpriteSize) {
            setSize(spriteHolder.getSpriteWidth() * PhysicsWorld.SCREEN_TO_BOX,
                    spriteHolder.getSpriteHeight() * PhysicsWorld.SCREEN_TO_BOX);
        }
        updateSpriteHolder(UpdateType.ALL);
    }

    protected void updateSpriteHolder(UpdateType updateType) {
        if(spriteHolder != null) {
            if(updateType == UpdateType.ALL) {
                spriteHolder.updateScale(getScaleX(), getScaleY());
            }
            if(updateType == UpdateType.ALL || updateType == UpdateType.POSSITION) {
                spriteHolder.updatePosition(getX() * PhysicsWorld.BOX_TO_SCREEN, getY() * PhysicsWorld.BOX_TO_SCREEN,
                        getRotation());
            }
            if(updateType == UpdateType.ALL || updateType == UpdateType.SIZE) {
                spriteHolder.updateSize(getWidth() * PhysicsWorld.BOX_TO_SCREEN, getHeight() * PhysicsWorld.BOX_TO_SCREEN);
            }
            if(updateType == UpdateType.ALL) {
                spriteHolder.updateOrigin(getOriginX() * PhysicsWorld.BOX_TO_SCREEN, getOriginY() * PhysicsWorld.BOX_TO_SCREEN);
            }
        }
    }

    protected void addChild(DrawObject object) {
        childArray.add(object);
    }

    protected boolean removeChild(DrawObject object) {
        return childArray.removeValue(object, true);
    }

    protected void clearChild() {
        childArray.clear();
    }
}
