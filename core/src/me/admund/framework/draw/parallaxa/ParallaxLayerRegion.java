package me.admund.framework.draw.parallaxa;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.draw.DrawObject;

public class ParallaxLayerRegion extends DrawObject {

    private Array<DrawObject> drawObjectList = null;

    public ParallaxLayerRegion() {
        this.drawObjectList = new Array<DrawObject>();
    }

    public void init(float posX, float posY, float width, float height) {
        setPosition(posX, posY);
        setSize(width, height);
    }

    public void updatePos(float layerTranspositionX, float layerTranspositionY) {
        setPosition(getX() + layerTranspositionX, getY() + layerTranspositionY);
        for(int i=0; i<drawObjectList.size; i++) {
            DrawObject tmp = drawObjectList.get(i);
            tmp.setPosition(tmp.getX() + layerTranspositionX, tmp.getY() + layerTranspositionY);
        }
    }

    public void add(DrawObject object) {
        if(!drawObjectList.contains(object, true)) {
            drawObjectList.add(object);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        for(int i=0; i<drawObjectList.size; i++) {
            drawObjectList.get(i).draw(batch, parentAlpha);
        }
    }
}
