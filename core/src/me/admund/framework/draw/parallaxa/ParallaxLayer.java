package me.admund.framework.draw.parallaxa;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import me.admund.framework.GameConfig;
import me.admund.framework.GameUtils;
import me.admund.framework.physics.PhysicsWorld;

public abstract class ParallaxLayer {
    protected boolean reusableRegions = true;

    private float value = 0f;
    private Vector2 regionSize = null;
    private Vector2 screenSize = null;

    private Vector2 cameraPos = null;
    private Vector2 layerStart = new Vector2(Vector2.Zero);
    private Vector2 layerCount = new Vector2(Vector2.Zero);

    private Array<ParallaxLayerRegion> regionList = new Array<ParallaxLayerRegion>();

    public ParallaxLayer(float regionSizeX, float regionSizeY, Vector2 cameraPos, float value) {
        this.value = value;
        this.regionSize = new Vector2(regionSizeX, regionSizeY);
        this.screenSize = new Vector2(GameConfig.GAME_WIDTH * PhysicsWorld.SCREEN_TO_BOX,
                GameConfig.GAME_HEIGHT * PhysicsWorld.SCREEN_TO_BOX);

        createLayer(cameraPos);
    }

    public abstract ParallaxLayerRegion create(float startPosX, float startPosY);

    public void updatePos(float cameraTranspositionX, float cameraTranspositionY) {
        float layerTranspositionX = cameraTranspositionX * value;
        float layerTranspositionY = cameraTranspositionY * value;

        cameraPos.add(cameraTranspositionX, cameraTranspositionY);

        updateRegions(layerTranspositionX, layerTranspositionY);
        if (reusableRegions) {
            reuseLayer();
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        for (int i = 0; i < regionList.size; i++) {
            regionList.get(i).draw(batch, parentAlpha);
        }
    }

    private void createLayer(Vector2 cameraPos) {
        this.cameraPos = new Vector2(cameraPos);

        layerStart.x = cameraPos.x - screenSize.x;
        layerStart.y = cameraPos.y - screenSize.y;

        layerCount.x = (int) Math.ceil(2 * screenSize.x / regionSize.x);
        if ((layerCount.x - 1) * regionSize.x < 1.5f * screenSize.x) {
            layerCount.x++;
        }
        layerCount.y = (int) Math.ceil(2 * screenSize.y / regionSize.y);
        if ((layerCount.y - 1) * regionSize.y < 1.5f * screenSize.y) {
            layerCount.y++;
        }

        for (int i = 0; i < layerCount.x; i++) {
            for (int j = 0; j < layerCount.y; j++) {
                regionList.add(create(layerStart.x + i * regionSize.x, layerStart.y + j * regionSize.y));
                GameUtils.mainLogger.info((layerStart.x + i * regionSize.x) + " " + (layerStart.y + j * regionSize.y));
            }
        }
    }

    private void updateRegions(float layerTranspositionX, float layerTranspositionY) {
        for (int i = 0; i < regionList.size; i++) {
            regionList.get(i).updatePos(layerTranspositionX, layerTranspositionY);
        }
    }

    private void reuseLayer() {
        for (int i = 0; i < regionList.size; i++) {
            ParallaxLayerRegion region = regionList.get(i);

            // niepotrzebny z lewej
            if (region.getX() + regionSize.x < cameraPos.x - screenSize.x) {
                region.updatePos(layerCount.x * regionSize.x, 0);
            }
            //niepotrzebny z prawej
            if (region.getX() > cameraPos.x + screenSize.x) {
                region.updatePos(-layerCount.x * regionSize.x, 0);
            }
            //niepotrzebny z dolu
            if (region.getY() + regionSize.y < cameraPos.y - screenSize.y) {
                region.updatePos(0, layerCount.y * regionSize.y);
            }
            //niepotrzebny z gory
            if (region.getY() > cameraPos.y + screenSize.y) {
                region.updatePos(0, -layerCount.y * regionSize.y);
            }
        }
    }
}
