package me.admund.framework.draw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.MathUtils;
import me.admund.framework.GameUtils;

public class DrawUtils {

    public static void draw(Batch batch, SpriteList spriteList, boolean alwaysVisible) {
        for(int i=0; i<spriteList.size; i++) {
            Sprite tmp = spriteList.get(i);
            if(alwaysVisible || isVisible(tmp)) {
                tmp.draw(batch);
            }
        }
    }

    public static TextureRegion getRandTextureFromTab(String[] stringTab) {
        return GameUtils.assetsManager.getTextureRegionFromMain(stringTab[MathUtils.random(stringTab.length - 1)]);
    }

    public static boolean isVisible(Sprite sprite) {
        if(GameUtils.currentCamera != null) {
            return GameUtils.currentCamera.frustum.boundsInFrustum(sprite.getX(), sprite.getY(), 0,
                    sprite.getWidth(), sprite.getHeight(), 0);
        }
        return true;
    }

    /**
     FreeTypeFontGenerator don't work with HTML build
     */

     public static BitmapFont getGeneratedFont(String fontFileName, int fontSize, int borderWidth) {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = fontSize;
        parameter.borderColor = Color.BLACK;
        parameter.borderWidth = borderWidth;
        return DrawUtils.getGeneratedFont(parameter, fontFileName);
    }

    public static BitmapFont getGeneratedFont(FreeTypeFontGenerator.FreeTypeFontParameter parameter, String fontFileName) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontFileName));
        BitmapFont newFont = generator.generateFont(parameter);
        generator.dispose();
        return newFont;
    }
}
