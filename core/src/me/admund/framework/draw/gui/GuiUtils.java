package me.admund.framework.draw.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import me.admund.framework.GameUtils;

public class GuiUtils {

    public static Drawable createSpriteDrawable(String atlasName, String textureName) {
        return new SpriteDrawable(new Sprite(GameUtils.assetsManager.getTextureRegion(atlasName, textureName)));
    }

    public static Drawable createSpriteDrawable(String textureName) {
        return new SpriteDrawable(new Sprite(GameUtils.assetsManager.getTextureRegionFromMain(textureName)));
    }

}
