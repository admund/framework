package me.admund.framework.draw.holders;

import com.badlogic.gdx.graphics.g2d.Sprite;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.animations.AnimationState;
import me.admund.framework.draw.sprites.OffsetSprite;

public class SimpleSpriteHolder extends AbstractSpriteHolder {

    public SimpleSpriteHolder(String textureName) {
        this(textureName, false);
    }

    public SimpleSpriteHolder(String textureName, boolean alwaysVisible) {
        this.alwaysVisible = alwaysVisible;
        addSprite(new Sprite(GameUtils.assetsManager.getTextureRegionFromMain(textureName)));
    }

    public SimpleSpriteHolder(String textureName, float offsetX, float offsetY) {
        this(textureName, offsetX, offsetY, false);
    }

    public SimpleSpriteHolder(String textureName, float offsetX, float offsetY, boolean alwaysVisible) {
        this.alwaysVisible = alwaysVisible;
        addSprite(new OffsetSprite(GameUtils.assetsManager.getTextureRegionFromMain(textureName),
                offsetX, offsetY));
    }

    @Override
    public void changeAnimationState(AnimationState state) {}

    @Override
    public void act(float delta) {}
}
