package me.admund.framework.draw.holders;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import me.admund.framework.GameUtils;
import me.admund.framework.draw.DrawUtils;
import me.admund.framework.draw.animations.AnimationState;

public class TiledSpriteHolder extends AbstractSpriteHolder {

    private float objectWidth = 0;
    private float objectHeight = 0;

    private float tileWidth = 0;
    private float tileHeight = 0;

    private float tileWidthCnt = 0;
    private float tileHeightCnt = 0;

    private float posX = 0;
    private float posY = 0;

    public TiledSpriteHolder(String textureName) {
        if(textureName != null) {
            addSprite(new Sprite(GameUtils.assetsManager.getTextureRegionFromMain(textureName)));
        }
    }

    @Override
    public void draw(Batch batch, float parentalAlpha) {
        Sprite sprite = getSpriteList().get(0);
        // TODO hack na niezgrywajace sie czesci
        //sprite.setSize(tileWidth + 2, tileHeight + 2);
        sprite.setSize(tileWidth, tileHeight);
        for (int i = 0; i < tileWidthCnt; i++) {
            for (int j = 0; j < tileHeightCnt; j++) {
                // TODO hack na niezgrywajace sie czesci
                //sprite.setPosition(posX + (i * tileWidth) - 1, posY + (j * tileHeight) - 1);
                sprite.setPosition(posX + (i * tileWidth), posY + (j * tileHeight));
                if (DrawUtils.isVisible(sprite)) {
                    sprite.draw(batch);
                }
            }
        }
    }

    @Override
    public void changeAnimationState(AnimationState state) {}

    @Override
    public void act(float delta) {}

    @Override
    public void updatePosition(float x, float y, float rotation) {
        super.updatePosition(x, y, rotation);
        posX = x;
        posY = y;
    }

    @Override
    public void updateSize(float width, float hight) {
        super.updateSize(width, hight);
        setObjectSize(width, hight);
    }

    public void setObjectSize(float objectWidth, float objectHight) {
        this.objectWidth = objectWidth;
        this.objectHeight = objectHight;
        calculateTileSize();
    }

    private void calculateTileSize() {
        tileWidthCnt = (int) (objectWidth / (getSpriteWidth()*10f));
        tileWidthCnt = tileWidthCnt == 0 ? 1 : tileWidthCnt;
        tileWidth = objectWidth / tileWidthCnt;

        tileHeightCnt = (int) (objectHeight / (getSpriteHeight()*10f));
        tileHeightCnt = tileHeightCnt == 0 ? 1 : tileHeightCnt;
        tileHeight = objectHeight / tileHeightCnt;

        tileWidth = optimize(tileWidth, tileHeight);
        tileWidthCnt = objectWidth / tileWidth;
        tileHeight = optimize(tileHeight, tileWidth);
        tileHeightCnt = objectHeight / tileHeight;
    }

    private float optimize(float tileSize1, float tileSize2) {
        float ratio = tileSize1/tileSize2;
        if(ratio >= 2) {
            int ratioInt = (int)ratio;
            return tileSize1/ratioInt;
        }
        return tileSize1;
    }
}