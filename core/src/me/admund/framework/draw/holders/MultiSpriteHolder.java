package me.admund.framework.draw.holders;

import me.admund.framework.draw.animations.AnimationState;

public class MultiSpriteHolder extends AbstractSpriteHolder {

    @Override
    public void changeAnimationState(AnimationState state) {}

    @Override
    public void act(float delta) {}
}
