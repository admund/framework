package me.admund.framework.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import me.admund.framework.GameUtils;
import me.admund.framework.achievements.IPlatformUtils;
import me.admund.framework.assets.FrameworkAssetsManager;
import me.admund.framework.draw.particle.FrameworkParticleManager;
import me.admund.framework.scenes.IScene;
import me.admund.framework.scenes.LoadingScene;
import me.admund.framework.scenes.ScenesManager;
import me.admund.framework.sounds.FrameworkSoundsManager;

public abstract class AbstractGame extends ApplicationAdapter {
    private boolean toDispose = false;
    private SpriteBatch batch = null;
    private boolean loadingAssets = true;
    private boolean lastUpdate = true;
    private LoadingScene loadingScene = null;

    protected Color clearColor = Color.BLACK;

    protected IPlatformUtils platformUtils = null;

    public AbstractGame(IPlatformUtils platformUtils) {
        this.platformUtils = platformUtils;
    }

    protected abstract FrameworkAssetsManager createAssetManager();
    protected abstract FrameworkSoundsManager createSoundsManager();
    protected abstract FrameworkParticleManager createParticleManager();

    @Override
    public void create () {
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);

        initAssetManager();

        ScenesManager.inst().push(createLoadingScene(), true);
        batch = new SpriteBatch();
    }

    @Override
    public void render () {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (toDispose) {
            return;
        }

        ScenesManager.inst().peek().act(Math.min(Gdx.graphics.getDeltaTime(), 1f / 30f), false);
        ScenesManager.inst().peek().draw(batch);

        if(loadingAssets) {
            loadingScene.updateProgress();
            if (GameUtils.assetsManager.update()) {
                if (lastUpdate) {
                    loadingScene.updateProgress();
                    lastUpdate = false;

                    GameUtils.assetsManager.init();
                    initSoundManager();
                    initParticleManager();
                } else {
                    loadingAssets = false;
                    ScenesManager.inst().pop();
                    ScenesManager.inst().push(createFirstScene(), true);
                }
            }
        }
        Gdx.gl.glFlush();
    }

    @Override
    public void resize(int width, int height) {
        ScenesManager.inst().peek().resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
        ScenesManager.inst().dispose();
        GameUtils.dispose();
    }

    public void setToDispose() {
        toDispose = true;
    }

    public abstract boolean backPressed();

    protected abstract IScene createFirstScene();

    protected IScene createLoadingScene() {
        if(loadingScene == null) {
            loadingScene = new LoadingScene();
        }
        return loadingScene;
    }

    private void initAssetManager() {
        FrameworkAssetsManager assetsManager = createAssetManager();
        assetsManager.load("loading.atlas", TextureAtlas.class);
        assetsManager.finishLoading();

        assetsManager.load();
        GameUtils.assetsManager = assetsManager;
    }

    private void initSoundManager() {
        GameUtils.soundsManager = createSoundsManager();
        GameUtils.soundsManager.init();
    }

    private void initParticleManager() {
        GameUtils.particleManager = createParticleManager();
        GameUtils.particleManager.init();
    }
}
