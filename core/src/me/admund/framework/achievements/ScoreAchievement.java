package me.admund.framework.achievements;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import me.admund.framework.logic.Score;

public class ScoreAchievement extends Achievement {
    private final static String SCORE_NAME = "scoreName";
    private final static String SCORE_VALUE = "scoreValue";

    protected Score score = null;
    protected String scoreName = null;
    protected float scoreValue = 0;

    public ScoreAchievement(String id) {
        this(id, "");
    }

    public ScoreAchievement(String id, String displayName) {
        super(id, displayName);
    }

    public Achievement init(Score score, String scoreName, float scoreValue) {
        this.score = score;
        this.scoreName = scoreName;
        this.scoreValue = scoreValue;
        return this;
    }

    @Override
    public boolean check() {
        return score.getScore(scoreName) >= scoreValue;
    }

    @Override
    public Json toJson(Json json) {
        super.toJson(json);
        json.writeValue(SCORE_NAME, scoreName);
        json.writeValue(SCORE_VALUE, scoreValue);
        return json;
    }

    @Override
    public void updateFromJson(JsonValue jsonData) {
        super.updateFromJson(jsonData);
        JsonValue child = jsonData.child();
        while (child != null) {
            if (SCORE_NAME.equals(child.name())) {
                scoreName = child.asString();
            } else if (SCORE_VALUE.equals(child.name())) {
                scoreValue = child.asInt();
            }
            child = child.next();
        }
    }
}
