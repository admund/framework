package me.admund.framework.achievements;

import com.badlogic.gdx.utils.Array;
import me.admund.framework.logic.GamePreferences;

public abstract class AchievementsManager {
    public static String ACHIEVEMENTS = "achievements";
    private GamePreferences gamePreferences = null;
    private AchievementList achievementsList = new AchievementList();
    private IPlatformUtils platformUtils;

    public AchievementsManager(IPlatformUtils platformUtils) {
        this.platformUtils = platformUtils;
    }

    public void init(GamePreferences gamePreferences) {
        this.gamePreferences = gamePreferences;
        platformUtils.createAchievements(this);
        String achievementsString = gamePreferences.getString(ACHIEVEMENTS);
        if (achievementsString != null) {
            updateAchievementsStateFromPreferences(achievementsString);
        } else {
            saveAchievementsState();
        }
        updateAchievementsStateFromProvider(platformUtils.getAchievementsStates());
    }

    public void addAchievement(Achievement achievement) {
        achievementsList.add(achievement);
    }

    public void checkAchievements() {
        for (Achievement achievement : achievementsList) {
            if (!achievement.isAchieved() && achievement.check()) {
                if (achievement.isIncremental()) {
                    ((IIncrementalAchievement) achievement).updateLastValue();
                    if (achievement.isAchieved()) {
                        platformUtils.incrementAchievement(achievement.getId(),
                                ((IIncrementalAchievement) achievement).getIncrementalProgress());
                    }
                } else {
                    platformUtils.earnAchievement(achievement.getId());
                    achievement.setAchieved(true);
                }
            }
        }
    }

    public void updateAchievementsStateFromProvider(Array<AchievementState> achievementStates) {
        for (AchievementState state : achievementStates) {
            for (Achievement achievement : achievementsList) {
                if (state.getId().equals(achievement.getId())) {
                    achievement.setAchieved(state.isAchieved());
                }
            }
        }
    }

    public void sendIncrementalAchievements() {
        for (Achievement achievement : achievementsList) {
            if (!achievement.isAchieved() && achievement.isIncremental()
                    && ((IIncrementalAchievement) achievement).getNotSendIncrementalProgress() > 0) {
                platformUtils.incrementAchievement(achievement.getId(),
                        ((IIncrementalAchievement) achievement).getNotSendIncrementalProgress());
                ((IIncrementalAchievement) achievement).updateLastSendValue();
            }
        }
    }

    public void saveAchievementsState() {
        gamePreferences.putString(ACHIEVEMENTS, achievementsList.toJson());
        gamePreferences.flush();
    }

    private void updateAchievementsStateFromPreferences(String achievementsString) {
        achievementsList.fromJson(achievementsString);
    }
}
