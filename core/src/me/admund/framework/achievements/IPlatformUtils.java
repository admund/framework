package me.admund.framework.achievements;

import com.badlogic.gdx.utils.Array;
import me.admund.framework.analytics.AnalyticEvent;

public interface IPlatformUtils {
    void signIn();
    void signOut();
    void rateGame();
    void showLeaderboard();
    void showAchievements();
    void createAchievements(AchievementsManager manager);
    boolean submitScore(int score, String guestName, String leaderboardName);
    boolean earnAchievement(String achievementId);
    boolean incrementAchievement(String achievementId, int incrementValue);
    Array<AchievementState> getAchievementsStates();
    void showScores();
    boolean isSignedIn();

    void vibrate();

    void sendAnalyticsEvent(AnalyticEvent event);

    boolean showFullScreenAd();
}
