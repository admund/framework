package me.admund.framework.achievements;


public interface IIncrementalAchievement {
    int getIncrementalProgress();
    int getNotSendIncrementalProgress();
    void updateLastValue();
    void updateLastSendValue();
}
