package me.admund.framework.achievements;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class IncrementScoreAchievement extends ScoreAchievement implements IIncrementalAchievement {
    private static final String LAST_VALUE = "lastValue";
    private static final String LAST_SEND_VALUE = "lastSendValue";
    private static final String INCREMENT_DIVISOR = "incrementDivisor";

    private int incrementDivisor = 1;
    private float lastSendValue = 0;
    private float lastValue = 0;

    public IncrementScoreAchievement(String id) {
        this(id, "");
    }

    public IncrementScoreAchievement(String id, String displayName) {
        super(id, displayName);
        setIncremental(true);
    }

    public IncrementScoreAchievement setIncrementDivisor(int incrementDivisor) {
        this.incrementDivisor = incrementDivisor;
        return this;
    }

    @Override
    public int getIncrementalProgress() {
        return getProgressDiff();
    }

    @Override
    public int getNotSendIncrementalProgress() {
        return (int)((lastValue - lastSendValue)/incrementDivisor);
    }

    @Override
    public void updateLastValue() {
        lastValue = score.getScore(scoreName);
        if (lastValue >= scoreValue) {
            setAchieved(true);
        }
    }

    @Override
    public void updateLastSendValue() {
        lastSendValue = lastValue;
    }

    @Override
    public boolean check() {
        return getProgressDiff() > 0;
    }

    public Json toJson(Json json) {
        super.toJson(json);
        json.writeValue(INCREMENT_DIVISOR, incrementDivisor);
        json.writeValue(LAST_VALUE, lastValue);
        json.writeValue(LAST_SEND_VALUE, lastSendValue);
        return json;
    }

    public void updateFromJson(JsonValue jsonData) {
        super.updateFromJson(jsonData);
        JsonValue child = jsonData.child();
        while (child != null) {
            if (INCREMENT_DIVISOR.equals(child.name())) {
                incrementDivisor = child.asInt();
            } else if (LAST_VALUE.equals(child.name())) {
                lastValue = child.asFloat();
            } else if (LAST_SEND_VALUE.equals(child.name())) {
                lastSendValue = child.asFloat();
            }
            child = child.next();
        }
    }

    private int getProgressDiff() {
        float diff = score.getScore(scoreName) - lastValue;
        return (int)(diff/incrementDivisor);
    }
}
