package me.admund.framework.achievements;

import com.badlogic.gdx.utils.*;

import java.io.StringWriter;

public class AchievementList extends Array<Achievement> {

    public String toJson() {
        StringWriter writer = new StringWriter();
        Json json = new Json();
        json.setWriter(writer);
        json.writeObjectStart();
        json.writeArrayStart(AchievementsManager.ACHIEVEMENTS);
        for (int i = 0; i < size; i++) {
            json.writeObjectStart();
            get(i).toJson(json);
            json.writeObjectEnd();
        }
        json.writeArrayEnd();
        json.writeObjectEnd();
        return writer.toString();
    }

    public void fromJson(String jsonString) {
        JsonReader reader = new JsonReader();
        JsonValue achievements =  reader.parse(jsonString).child();
        JsonValue child = achievements.child();
        while (child != null) {
            String id = child.get(Achievement.ID).asString();
            Achievement achievement = findById(id);
            if (achievement != null) {
                achievement.updateFromJson(child);
            }
            child = child.next();
        }
    }

    private Achievement findById(String id) {
        for (Achievement achievement : this) {
            if (achievement.getId().equals(id)) {
                return achievement;
            }
        }
        return null;
    }
}
