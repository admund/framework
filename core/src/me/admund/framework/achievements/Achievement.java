package me.admund.framework.achievements;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public abstract class Achievement {
    public static final String ID = "id";
    private static final String DISPLAY_NAME = "displayName";
    private static final String IS_ACHIEVED = "isAchieved";
    private static final String IS_INCREMENTAL = "isIncremental";

    private String id = null;
    private String displayName = null;
    private boolean isAchieved = false;
    private boolean isIncremental = false;

    public Achievement(String id, String displayName) {
        this.id = id;
        this.displayName = displayName;
    }

    public boolean check() {
        return false;
    }

    public String getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isAchieved() {
        return isAchieved;
    }

    public void setAchieved(boolean isAchieved) {
        this.isAchieved = isAchieved;
    }

    public boolean isIncremental() {
        return isIncremental;
    }

    public Json toJson(Json json) {
        json.writeValue(ID, id);
        json.writeValue(DISPLAY_NAME, displayName);
        json.writeValue(IS_ACHIEVED, isAchieved);
        json.writeValue(IS_INCREMENTAL, isIncremental);
        return json;
    }

    public void updateFromJson(JsonValue jsonData) {
        JsonValue child = jsonData.child();
        while (child != null) {
            if (DISPLAY_NAME.equals(child.name())) {
                displayName = child.asString();
            } else if (IS_ACHIEVED.equals(child.name())) {
                isAchieved = child.asBoolean();
            } else if (IS_INCREMENTAL.equals(child.name())) {
                isIncremental = child.asBoolean();
            }
            child = child.next();
        }
    }

    protected void setIncremental(boolean isIncremental) {
        this.isIncremental = isIncremental;
    }
}
