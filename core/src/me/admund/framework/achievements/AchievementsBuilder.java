package me.admund.framework.achievements;

public abstract class AchievementsBuilder {
    public abstract void addAchievements(AchievementsManager manager);
}
