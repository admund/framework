package me.admund.framework.scenes;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Disposable;

public interface IScene extends Disposable {
    void act(float deltaTime, boolean isPause);
    void draw(Batch batch);
    void resize(int width, int height);
    void refresh();
    void create();
}
