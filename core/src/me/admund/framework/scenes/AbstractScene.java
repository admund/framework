package me.admund.framework.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import me.admund.framework.GameConfig;
import me.admund.framework.physics.PhysicsWorld;

public abstract class AbstractScene implements IScene {
    protected Stage guiStage = null;
    //protected IGamepadMenuElement firstElement = null; TODO add menu control by gamepad
    private Stage stage = null;
    private ObjectMap<String, Group> groupList = null;

    public AbstractScene() {
        stage = new Stage(new ExtendViewport(GameConfig.GAME_WIDTH, GameConfig.GAME_HEIGHT));
        guiStage = new Stage(new ExtendViewport(GameConfig.GAME_WIDTH, GameConfig.GAME_HEIGHT));
        groupList = new ObjectMap<String, Group>();
        setInputProcessor();
    }

    public void createGroups(String[] groupNames) {
        for (String groupName : groupNames) {
            Group group = new Group();
            groupList.put(groupName, group);
            stage.addActor(group);
        }
    }

    public void addActor(String groupName, Actor actor) {
        Group group = groupList.get(groupName);
        if (group == null) {
            throw new RuntimeException("AbstractScene: Can't find group named " + groupName);
        }
        group.addActor(actor);
    }

    public Group getGroup(String groupName) {
        Group group = groupList.get(groupName);
        if (group == null) {
            throw new RuntimeException("AbstractScene: Can't find group named " + groupName);
        }
        return group;
    }

    public int getActorCount() {
        return stage.getActors().size;
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, false);
        guiStage.getViewport().update(width, height, false);
        PhysicsWorld.BOX_SCREEN_WIDTH = stage.getWidth() * PhysicsWorld.SCREEN_TO_BOX;
        PhysicsWorld.BOX_SCREEN_HEIGHT = stage.getHeight() * PhysicsWorld.SCREEN_TO_BOX;
    }

    @Override
    public void act(float deltaTime, boolean isPause) {
        if (!isPause) {
            stage.act(deltaTime);
        }
        guiStage.act(deltaTime);
    }

    @Override
    public void draw(Batch batch) {
        stage.draw();
        guiStage.draw();
    }

    @Override
    public void refresh() {
        setInputProcessor();
    }

    @Override
    public void dispose() {
        stage.dispose();
        guiStage.dispose();
    }

    public Camera getCurrentCamera() {
        return stage.getCamera();
    }

    protected void setInputProcessor() {
        InputMultiplexer input = new InputMultiplexer();
        input.addProcessor(guiStage);
        input.addProcessor(stage);
        Gdx.input.setInputProcessor(input);
    }
}
