package me.admund.framework.client;

import com.badlogic.gdx.utils.Array;
import me.admund.framework.achievements.AchievementState;
import me.admund.framework.achievements.AchievementsManager;
import me.admund.framework.achievements.IPlatformUtils;

public class HtmlPlatformUtils implements IPlatformUtils {
    private static final int GAME_ID = -1; // TODO add Your gameId
    private static final String PRIVATE_KEY = null; // TODO add Your private kay

    private GJAPI gjapi = null;

    public HtmlPlatformUtils() {
        gjapi = new GJAPI(PRIVATE_KEY, GAME_ID);
    }

    @Override
    public void signIn() {

    }

    @Override
    public void signOut() {

    }

    @Override
    public void rateGame() {

    }

    @Override
    public void showLeaderboard() {

    }

    @Override
    public void showAchievements() {

    }

    @Override
    public void createAchievements(AchievementsManager manager) {

    }

    @Override
    public boolean submitScore(int score, String guestName, String leaderboardName) {
        return gjapi.addScore(score, guestName);
    }

    @Override
    public boolean earnAchievement(String achievementId) {
        return false;
    }

    @Override
    public boolean incrementAchievement(String achievementId, int incrementValue) {
        return false;
    }

    @Override
    public Array<AchievementState> getAchievementsStates() {
        return null;
    }

    @Override
    public void showScores() {
    }

    @Override
    public boolean isSignedIn() {
        return false;
    }

    @Override
    public void vibrate() {
        // DO NOTHING
    }

    @Override
    public void sendAnalyticsEvent(String eventName) {

    }

    @Override
    public boolean showFullScreenAd() {
        return false;
    }
}
