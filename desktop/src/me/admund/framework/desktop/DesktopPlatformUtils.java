package me.admund.framework.desktop;

import com.badlogic.gdx.utils.Array;
import me.admund.framework.achievements.AchievementState;
import me.admund.framework.achievements.AchievementsBuilder;
import me.admund.framework.achievements.AchievementsManager;
import me.admund.framework.achievements.IPlatformUtils;
import me.admund.framework.analytics.AnalyticEvent;
import org.gamejolt.GameJoltAPI;
import org.gamejolt.Trophy;

import java.util.ArrayList;

public class DesktopPlatformUtils implements IPlatformUtils {
    private static final int GAME_ID = -1;
    private static final String PRIVATE_KEY = "TODO";

    private AchievementsBuilder achievementsBuilder = null;
    private GameJoltAPI api = null;

    public DesktopPlatformUtils(AchievementsBuilder achievementsBuilder) {
        api = new GameJoltAPI(GAME_ID, PRIVATE_KEY);
        this.achievementsBuilder = achievementsBuilder;
    }

    @Override
    public void signIn() {}

    @Override
    public void signOut() {}

    @Override
    public void rateGame() {}

    @Override
    public void showLeaderboard() {}

    @Override
    public void showAchievements() {}

    @Override
    public void createAchievements(AchievementsManager manager) {
        achievementsBuilder.addAchievements(manager);
    }

    @Override
    public boolean submitScore(int score, String guestName, String leaderboardName) {
        if (api.isVerified()) {
            return api.addHighscore(score + " pts", score);
        } else {
            return api.addHighscore(guestName == null ? "Guest" : guestName, score + " pts", score);
        }
    }

    @Override
    public boolean earnAchievement(String achievementId) {
        return api.isVerified() && api.achieveTrophy(Integer.parseInt(achievementId));
    }

    @Override
    public boolean incrementAchievement(String achievementId, int incrementValue) {
        return false; // GameJolt don't provide increment achievements
    }

    @Override
    public void showScores() {}

    @Override
    public boolean isSignedIn() {
        return false;
    }

    @Override
    public void vibrate() {
        // DO NOTHING
    }

    @Override
    public void sendAnalyticsEvent(AnalyticEvent event) {
        // TODO add some desktop analytics
        System.out.println(event);
    }

    @Override
    public boolean showFullScreenAd() {
        // TODO add some desktop adds (?)
        return false;
    }

    @Override
    public Array<AchievementState> getAchievementsStates() {
        Array<AchievementState> achievementsList = new Array<AchievementState>();
        ArrayList<Trophy> trophyArrayList = api.getTrophies();

        if (trophyArrayList != null) {
            for (Trophy trophy : trophyArrayList) {
                achievementsList.add(parse(trophy));
            }
        }
        return achievementsList;
    }

    private AchievementState parse(Trophy trophy) {
        return new AchievementState(trophy.getId(), isAchieved(trophy));
    }

    private boolean isAchieved(Trophy trophy) {
        return trophy.isAchieved();
    }
}
